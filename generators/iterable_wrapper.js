// Generators' main points

// 1. Generator methods have a
// new *<...> syntax inside a class,
// and Generator functions have
// the syntax function * myGenerator(){}

// 2. Calling generators myGenerator()
// returns a generator object that also
// implements the iterator rules

// 3. Generators use a speacial 'yield'
// statement to return data

// 4. yield statements keep track
// of previous calls and simply continue
// from where it left off

// 5. If you use yield inside a loop,
// it'll only execute once each time
// we call the next() method on iterator

class Users {
  constructor(users) {
    this.users = users;
    this.len = users.length;
  }

  *getIterator() {
    for (let index = 0; index < this.len; index++) {
      yield this.users[index];
    }
  }
}

const allUsers = new Users([
  { name: 'ahmed' },
  { name: 'salah' },
  { name: 'tarak' }
]);

const allUsersIterator = allUsers.getIterator();

//console.log([...allUsersIterator]);


console.log(allUsersIterator.next());
console.log(allUsersIterator.next());
console.log(allUsersIterator.next());
console.log(allUsersIterator.next());

for (const u of allUsersIterator) {
  console.log(u.name);
}

console.log([...allUsersIterator]);

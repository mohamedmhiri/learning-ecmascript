function* myFun() {
  const name = 'tarak';
  yield name;

  const lastName = 'morik';
  yield lastName;
}

const myIter = myFun();

console.log(myIter.next());
console.log(myIter.next());
console.log(myIter.next());

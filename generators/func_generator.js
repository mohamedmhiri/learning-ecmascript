function* Users(users) {
  for (let u in users) {
    yield users[u++];
  }
}

const allUsers = Users([
  { name: 'ahmed' },
  { name: 'salah' },
  { name: 'tarak' }
]);

console.log(allUsers.next());
console.log(allUsers.next());
console.log(allUsers.next());
console.log(allUsers.next());

for (const u of allUsers) {
  console.log(u.name);
}

console.log([...allUsers]);

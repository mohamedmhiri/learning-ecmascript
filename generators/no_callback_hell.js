const data = require('./data.json');

const findById = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data.filter(item => item.id == id))
    }, 1000);
  })
}
const entity = {
  * gen() {
  let postNbr50 = yield findById(50);
  let postNbr100 = yield findById(100);
  console.log(postNbr50, postNbr100);
  }
};

entity.gen().next();

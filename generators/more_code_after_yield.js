function* myGen() {
  const name = 'tarak';
  yield name;
  console.log('more code');  
}

const myIter = myGen();

console.log(myIter.next());
console.log(myIter.next());

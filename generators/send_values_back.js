function* profileGen() {
  let answer = yield 'How old Are you ?';

  if (answer > 18) {
    yield 'Adult';
  } else {
    yield 'Child';
  }
  let t = answer;
  yield t;
}

const iter = profileGen();

console.log(iter.next());
console.log(iter.next(24));
console.log(iter.next(20));
console.log(iter.next());
console.log(iter.next());

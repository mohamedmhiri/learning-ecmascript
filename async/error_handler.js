(async function() {
  var sleep = function(params) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(params * params)
      }, 1000);
    })
  }

  var errSleep = function(params) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        reject('Error Sleep')
      }, 1000);
    })
  }

  try {
    var res1 = await sleep(1);
    var res2 = await errSleep(4);
    var res3 = await sleep(1);

    console.log('res1: ',res1);
    console.log('res2: ',res2);
    console.log('res3: ',res3);
    
  } catch (error) {
    console.log('err: ', error);
    console.log('res1: ',res1);
    console.log('res2: ',res2);
    console.log('res3: ',res3);
  }
})();
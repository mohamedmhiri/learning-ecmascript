(async function() {
  var sleep = function(params) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(params * params)
      }, 1000);
    })
  }

  for (let i = 0; i < 3; i++) {
    const result = await sleep(i);

    for (let j = 0; j < result; j++) {
      console.log(`i: ${i}, j: ${j}`, await sleep(j));      
    }
    
  }
})();
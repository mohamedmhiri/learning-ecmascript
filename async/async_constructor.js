'use strict';

// Nodejs
console.log(async function () {});

// Chrome or Firefox
console.log(async function () {}.constructor);
console.log(async function () {}.__proto__);
(async function() {

  var sleep = function(params) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        resolve(params * params)        
      }, 1000)
    })
  }

  // async await way
  var result = await sleep(2);
  console.log(result);
  // Promise way
  sleep(3).then(res => console.log(res))
  
  var res1 = await sleep(2);
  var res2 = await sleep(res1);
  var res3 = await sleep(res2);
  console.log(res3);
  
})();
(async function() {
  
  var sleep = function(params) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(params * params)
      }, 1000);
    })
  }

  var results = await Promise.all([sleep(1), sleep(2)])
  console.log(results);
  
})();
class Users {
  constructor(users) {
    this.users = users;
  }

  [Symbol.iterator]() {
    let i = 0;
    let users = this.users;

    // this returned object is called an "iterator"
    return {
      next() {
        if (i < users.length) {
          return { done: false, value: users[i++] };
        }
        return { done: true };
      }
    }
  }
}

const allUsers = new Users([
  { name: 'ahmed' },
  { name: 'tarak' },
  { name: 'khaled' }
]);

const allUsersIterator = allUsers[Symbol.iterator]();

console.log(allUsersIterator.next());
console.log(allUsersIterator.next());
console.log(allUsersIterator.next());

for (const iterator of allUsers) {
  console.log(iterator.name);
}

console.log([...allUsers]);

class String {
  constructor(data) {
    this.data = data;
  }

  search(obj) {
    obj[Symbol.search](this.data);
  }
}

class RegExp {
  constructor(val) {
    this.val = val;
  }

  [Symbol.search](str) {
    return str.indexOf(this.val);
  }
}

class Product {
  constructor(item) {
    this.item = item
  }

  [Symbol.search](val) {
    return val.indexOf(this.item) >= 0 ? 'Found': 'Not Found';
  }
}

const obj = new Product('soap');


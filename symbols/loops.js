let obj = {};
obj['prop1'] = 1;
obj['prop2'] = 2;


const prop3 = Symbol('prop3');
const prop4 = Symbol('prop4');

obj[prop3] = 3;
obj[prop4] = 4;

for (const key in obj) {
  console.log(key, '=', obj[key]);
}

console.log(obj[prop3]);
console.log(obj[prop4]);

const includes = Symbol('some data');

Array.prototype[includes] = () => console.log('inside includes func');

const arr = [1, 2, 3];

console.log(arr.includes(1)); // true
console.log(arr['includes'](1)); // true

console.log(arr[includes]()); // 'inside includes func'

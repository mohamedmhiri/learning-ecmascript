const sym1 = Symbol();
const sym2 = Symbol('txt');
const sym3 = Symbol('txt');
const sym4 = Symbol.for('txt');
const sym5 = Symbol.for('txt');
const sym6 = Symbol.for();

console.log(sym1 == sym6);
console.log(sym2 == sym3);
console.log(sym4 == sym5);
console.log(sym2 == sym4);
console.log(sym2 == sym5);
console.log(sym3 == sym4);
console.log(sym3 == sym5);
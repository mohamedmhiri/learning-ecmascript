const obj = {name: 'mohamed'};

obj[Symbol('age')] = 24;
obj[Symbol('job')] = () => console.log('software engineer');

console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));

let baby = {name: 'A baby name'};
let type = Symbol('age');

baby[type] = '16 months';

let cry = Symbol('Crying');
baby[cry] = () => 'Aaaaaaa';

try {
  console.log(baby.type);
  console.log(baby[type]);
  // console.log(baby.cry());
  console.log(baby[cry]());
  
} catch (err) {
  console.log(err);
  console.log(baby.type);
  console.log(baby[type]);
  console.log(baby.cry());
  console.log(baby[cry]());
  
}
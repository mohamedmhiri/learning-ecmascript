'use strict';

function fetchJson(url) {
  return new Promise((resolve, reject) => {
    const data = require(url);
    resolve(data);
  })
};

fetchJson('./../../generators/data.json')
  .then(data => console.log(data))
  .catch(err => console.log(err))
  .finally(res => console.log(res));

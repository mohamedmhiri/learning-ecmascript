'use strict';

const simplePromise = new Promise(/* executor function */(resolve, reject) => {
  resolve('Companion');
  reject('An error');
});

simplePromise
.then(item => {
  console.log(item === 'Companion');
  return item + ' n°1';
}, err => {
  console.error('error');
})
.then(result => console.log(result))
.catch(err => console.log(err));

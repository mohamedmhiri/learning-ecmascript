'use strict';

const fetchJson = (url) => {
  return new Promise((resolve, reject) => {
    resolve(require(url));
  });
}



Promise.race([
  fetchJson('./data2.json'),
  fetchJson('./data1.json'),
  fetchJson('./data3.json')
])
  .then(data => {
    console.log(data)
  })
  .catch(err => console.error(err));

'use strict';

console.log('Initiating ...');

const delayedPromise = new Promise((resolve, reject) => {
  console.log('Entering delayed promise ...');
  setTimeout(() => {
    resolve('Executing delayed ...');
  }, 1000);
  reject('So many delayed error ...');
});

delayedPromise
  .then(data => console.log(data))
  .catch(err => console.error(err));

console.log('In progress ...');

const immediatePromise = new Promise((resolve, reject) => {
  console.log('Entering immediate promise ...');
  resolve('Executing immediate ...');
  reject('So many immediate error ...');
});

immediatePromise
  .then(data => console.log(data))
  .catch(err => console.error(err));

console.log('finishing ...');

'use strict';

const fetchJson = (url) => {
  return new Promise((resolve, reject) => {
    resolve(require(url));
  });
}


Promise.all([
  fetchJson('./data1.json'),
  fetchJson('./data2.json'),
  fetchJson('./data3.json')
])
  .then(data => {
    console.log(data[0][0])
    console.log(data[1][0])
    console.log(data[2][0])
  })
  .catch(err => console.error(err));

'use strict';

function* DomTraversal(element) {
  yield element;
  element = element.firstElementChild;
  while (element) {
    yield* DomTraversal(element);
    element = element.nextElementSibling;
  }
}

const first = document.getElementById('subTree');
for (const iterator of DomTraversal(first)) {
  console.log(iterator.nodeName);
  
}
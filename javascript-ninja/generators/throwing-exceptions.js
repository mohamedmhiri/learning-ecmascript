'use strict';

function* BasicGenerator() {
  try {
    yield 'Basic1';
    console.log('txt');
  } catch (e) {
    console.log(e === 'error', 'problems...');
  }
}

const basic = BasicGenerator();
let res = basic.next();
console.log(res.value === 'Basic1');

basic.throw('error');

'use strict';

function* IdGenerator() {
  let id = 0;
  while(1) {
    yield ++id;
  }
}
/*
for (const iterator of IdGenerator()) {
  console.log(iterator);
}
*/

let item;
const idGenerator = IdGenerator();
while (!(item = idGenerator.next()).done && item.value <20) {
  console.log(item);
  
}
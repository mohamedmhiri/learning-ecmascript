function oldWay(element, callback) {
  callback(element);
  element = element.firstElementChild;
  while (element) {
    oldWay(element, callback);
    element = element.nextElementSibling;
  }
}

const subTree = document.getElementById('subTree');

oldWay(subTree, function(element) {
  console.log(element.nodeName);
  
})
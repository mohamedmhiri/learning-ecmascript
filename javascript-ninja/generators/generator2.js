'use strict';

function* WarriorGenerator() {
  yield 'companion1';
  yield* MoreCompanions();
  yield 'companion4';
  yield 'companion5';
}

function* MoreCompanions() {
  yield 'companion2';
  yield 'companion3';
}

for (const iterator of WarriorGenerator()) {
  console.log(iterator);  
}
'use strict';

function* WeaponGenerator() {
  yield 'Sword';
  yield 'knife';
  yield 'pistol';
  yield 'bombs';
}

const weaponIterator = WeaponGenerator();
let item;
while (!(item = weaponIterator.next()).done) {
  console.log(item);
}

for (const iterator of WeaponGenerator()) {
  console.log(iterator);  
}
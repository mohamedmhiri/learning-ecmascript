'use strict';

function* FoodGenerator(action) {
  const meal = yield (action + ' arg');
  console.log(meal === 'Second', 'Second');
  yield ('given ' + meal + ' after ' + action);
}

const foodIterator = FoodGenerator('First');

const res1 = foodIterator.next();
console.log(res1.value === 'First arg', 'First arg');

const res2 = foodIterator.next('Second');
console.log(res2.value === 'given Second after First', 'Stop');

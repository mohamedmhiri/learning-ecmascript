'use strict';

const fetchJson = (url) => {
  return new Promise((resolve, reject) => {
    resolve(require(url));
  });
}

function async(generator) {
  let iterator = generator();

  function handle(iteratorRes) {
    if (iteratorRes.done) {
      return;
    }
    const iteratorVal = iteratorRes.value;

    if (iteratorVal instanceof Promise) {
      iteratorVal
        .then(data => handle(iterator.next(data)))
        .catch(err => iterator.throw(err));
    }
  }

  try {
    handle(iterator.next())
  } catch (error) {
    iterator.throw(error)
  }
}

async(function* () {
  try {
    const data1 = yield fetchJson('../promises/data1.json');
    const data2 = yield fetchJson('../promises/data2.json');
    const data3 = yield fetchJson('../promises/data3.json');
    console.log(data1);
    console.log(data2);
    console.log(data3);
  } catch (error) {
    console.error(error);
  }
});
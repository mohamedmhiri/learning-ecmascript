'use strict';

const fetchJson = (url) => {
  return new Promise((resolve, reject) => {
    resolve(require(url));
  });
}

(async function() {
  try {
    const data1 = await fetchJson('../promises/data1.json');
    const data2 = await fetchJson('../promises/data2.json');
    console.log(data1);
    console.log(data2);
    
  } catch (error) {
    console.error(error);
    
  }
})();